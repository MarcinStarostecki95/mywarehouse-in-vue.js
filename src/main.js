import Vue from "vue";
import App from "./App.vue";
import router from "./_helpers/router";
import { store } from "./_store";
import VeeValidate from "vee-validate";

import MaterialKit from "./plugins/material-kit";

Vue.config.productionTip = false;
// Vue.config.performance = true;

Vue.use(MaterialKit);

const NavbarStore = {
  showNavbar: false
};

const isDev = process.env.NODE_ENV !== "production";
Vue.config.performance = isDev;

Vue.use(VeeValidate);
Vue.use(require("vue-moment"));

//Vue.material.locale.dateFormat = 'DD/MM/YYYY'

Vue.mixin({
  data() {
    return {
      NavbarStore
    };
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
