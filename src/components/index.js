import DropDown from "./Dropdown.vue";
import Parallax from "./Parallax.vue";
import Pagination from "./Pagination.vue";
import Slider from "./Slider.vue";
import Badge from "./Badge.vue";
import NavTabsCard from "./Cards/NavTabsCard.vue";
import LoginCard from "./Cards/LoginCard.vue";
import RegisterCard from "./Cards/RegisterCard.vue";
import Tabs from "./Tabs.vue";
import Modal from "./Modal.vue";

export {
  DropDown,
  Parallax,
  Pagination,
  Slider,
  Badge,
  NavTabsCard,
  LoginCard,
  RegisterCard,
  Tabs,
  Modal
};
