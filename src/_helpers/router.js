import Vue from "vue";
import Router from "vue-router";
import Index from "../views/Index.vue";
import Landing from "../views/Landing.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import Profile from "../views/Profile.vue";
import MainNavbar from "../layout/MainNavbar.vue";
import MainFooter from "../layout/MainFooter.vue";
import Search from "../views/Search.vue";
import OfferDetails from "../views/OfferDetails.vue";
import RentForm from "../views/RentForm.vue";
import ReservationLanding from "../views/ReservationLanding.vue";
import RentalSuccessPage from "../views/RentalSuccessPage.vue";
import AdminGetProduct from "../views/AdminGetProduct.vue";
import AdminGetUsers from "../views/AdminGetUsers.vue";
import AdminPanel from "../views/AdminPanel.vue";
import AllReservations from "../views/AllReservations.vue";
import EditProduct from "../views/EditProduct.vue";
import EditUser from "../views/EditUser.vue";
import EditReservation from "../views/EditReservation.vue";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "index",
      components: { default: Index, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/landing",
      name: "landing",
      components: { default: Landing, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/login",
      name: "login",
      components: { default: Login, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path:"/admingetproduct",
      name:"admingetproduct",
      components: { default: AdminGetProduct, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path:"/admingetusers",
      name:"admingetusers",
      components: { default: AdminGetUsers, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path:"/allreservations",
      name:"allreservations",
      components: { default: AllReservations, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path:"/adminpanel",
      name:"adminpanel",
      components: { default: AdminPanel, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: "/register",
      name: "register",
      components: { default: Register, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: "/search",
      name: "search",
      components: { default: Search, header: MainNavbar, footer: MainFooter },
      props: {
        default: true,
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/offerdetails/:id",
      name: "offerdetails",
      components: {
        default: OfferDetails,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/editproduct/:id",
      name: "editproduct",
      components: {
        default: EditProduct,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/edituser/:id",
      name: "edituser",
      components: {
        default: EditUser,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/editreservation/:id",
      name: "editreservation",
      components: {
        default: EditReservation,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 100 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/rentform/:id",
      name: "rentform",
      components: {
        default: RentForm,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/profile",
      name: "profile",
      components: { default: Profile, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/reservationlanding",
      name: "ReservationLanding",
      components: {
        default: ReservationLanding,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/rentalsuccesspage",
      name: "RentalSuccessPage",
      components: {
        default: RentalSuccessPage,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
